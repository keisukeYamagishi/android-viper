package com.example.viper

class MainIntractor {

    fun githubApi(keyword: String, completion: (items: Array<String>) -> Unit ) {
        val uri = BuildQuery(keyword = keyword).build()

        val githubClient = HttpClient(url = uri)
        githubClient.githubApi { json ->
            val model = this.toModel(json)
            completion(this.emurateString(model))
        }
    }

    private fun toModel(json: String): Responce {
        val perser = Perse()
        return perser.toModel(json)
    }

    private fun emurateString(responce: Responce): Array<String> {
        var items: Array<String> = arrayOf()
        for (item in responce.items) {
            items += item.full_name
        }
        return items
    }
}