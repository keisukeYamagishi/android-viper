package com.example.viper

interface  MainPresentation {
    fun onCreate()
    fun inputText(text: String)
}

class MainPresenter (view: MainView, intaractor: MainIntractor): MainPresentation {

    val intaractor: MainIntractor
    val view: MainView
    var searchText: String = "HttpSession"
    var items: Array<String> = arrayOf()

    init {
        this.intaractor = intaractor
        this.view = view
    }

    override fun onCreate() {
        this.intaractor.githubApi(keyword = this.searchText) {items ->
            this.items = items
            this.view.updateRepositries(this.items)
        }
    }

    override fun inputText(text: String) {
        this.intaractor.githubApi(keyword = text) {items ->
            this.items = items
            this.view.updateRepositries(this.items)
        }
    }
}