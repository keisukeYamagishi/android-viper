package com.example.viper

import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity

interface MainView {
    fun updateRepositries(items: Array<String>)
}

class MainActivity : AppCompatActivity(), MainView {

    lateinit var presenter: MainPresenter
    lateinit var adapater: ArrayAdapter<String>
    lateinit var listView: ListView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        this.listView = findViewById<ListView>(R.id.list_view)
        this.adapater = ArrayAdapter(this, R.layout.list, arrayListOf())
        this.presenter = MainPresenter(view = this, intaractor = MainIntractor())
        this.presenter.onCreate()
    }

    override fun updateRepositries(items: Array<String>){
        println("updateRepositries ")
        this.adapater = ArrayAdapter(this, R.layout.list, items)
        runOnUiThread {
            this.listView.adapter = this.adapater
        }
    }
}