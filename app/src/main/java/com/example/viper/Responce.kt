package com.example.viper

data class Responce (
    val items: Array<Repositories>
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Responce

        if (!items.contentEquals(other.items)) return false

        return true
    }

    override fun hashCode(): Int {
        return items.contentHashCode()
    }
}

data class Repositories (
    val id: Int,
    val name: String,
    val full_name: String,
    val html_url: String
)