package com.example.viper

import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.json.responseJson

class HttpClient(url: String) {
    val url: String
    init {
        this.url = url
    }

    fun githubApi(completion: (result: String) -> Unit ) {

        Fuel.get(this.url).responseJson { request, response, result ->
            if (response.statusCode == 200) {
                val json = result.get().content
                completion(json)
            }
        }
    }
}