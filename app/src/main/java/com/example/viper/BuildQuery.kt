package com.example.viper

class BuildQuery(keyword: String) {
    val keyword: String
    val domain: String = "https://api.github.com"
    val endPoint: String = "/search/repositories"
    
    init {
        this.keyword = keyword
    }
    
    fun build(): String {
        return domain + endPoint + "?q=" + this.keyword
    }
}